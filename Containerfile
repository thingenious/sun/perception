# BASE_IMAGEs:
# depending on the cuda version (1x.y)
# https://catalog.ngc.nvidia.com/orgs/nvidia/containers/cuda/tags

# nvcr.io/nvidia/cuda:12.3.0-devel-ubuntu22.04
# nvcr.io/nvidia/cuda:12.2.0-devel-ubuntu22.04
# nvcr.io/nvidia/cuda:12.1.1-cudnn8-devel-ubuntu22.04
ARG BASE_IMAGE=nvcr.io/nvidia/cuda:12.3.2-devel-ubuntu22.04
FROM --platform=linux/amd64 $BASE_IMAGE


# environment variables
ENV TZ=UTC
# make sure python output is sent straight to terminal
ENV PYTHONUNBUFFERED 1
# make sure apt doesn't ask questions
ENV DEBIAN_FRONTEND="noninteractive"
ENV DEBCONF_NONINTERACTIVE_SEEN true

# install dependencies
RUN apt update && \
    apt install -y --no-install-recommends \
    libpython3.10 \
    python3-pip \
    python3-dev \
    curl \
    tzdata \
    locales \
    ca-certificates \
    openssl \
    tini && \
    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8 && \
    rm -rf /var/lib/apt/lists/* && \
    rm -rf /var/cache/apt/archives/*

# install system-wide, else pytriton will complain
# (no module named numpy/zmq)
RUN pip install --upgrade pip wheel setuptools numpy zmq

# add non-root user
ARG GROUP_ID=1000
ENV GROUP_ID=${GROUP_ID}
RUN addgroup --system --gid ${GROUP_ID} user
ARG USER_ID=1000
ENV USER_ID=${USER_ID}

RUN adduser --disabled-password --gecos '' --shell /bin/bash --uid ${USER_ID} --gid ${GROUP_ID} user
RUN mkdir -p /home/user/.local/bin && \
    echo 'PATH=/home/user/.local/bin:$PATH' >> /home/user/.bashrc && \
    chown -R user:user /home/user
ENV PATH=/home/user/.local/bin:${PATH}

# continue as the new user
USER user
RUN python3 -m pip install --upgrade pip setuptools wheel


# python requirements
COPY --chown=user:user requirements/main.txt /tmp/requirements.txt
RUN pip install --user -r /tmp/requirements.txt && \
    rm /tmp/requirements.txt

# ports
# the defauls that pytriton uses
ARG HTTP_PORT=8000
ENV HTTP_PORT=$HTTP_PORT
ARG GRPC_PORT=8001
ENV GRPC_PORT=$GRPC_PORT
ARG METRICS_PORT=8002
ENV METRICS_PORT=$METRICS_PORT
ARG SAGEMAKER_PORT=8080
ENV SAGEMAKER_PORT=$SAGEMAKER_PORT

EXPOSE $HTTP_PORT
EXPOSE $GRPC_PORT
EXPOSE $METRICS_PORT
EXPOSE $SAGEMAKER_PORT

# pt model
COPY --chown=user:user models /home/user/models

# app
COPY --chown=user:user app /home/user/app
WORKDIR /home/user

ENV TINI_SUBREAPER=true

# entrypoint
ENTRYPOINT ["/usr/bin/tini", "--"]
CMD ["python3", "-m", "app" ]
