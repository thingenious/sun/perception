[project]
name = 'sun-ml'
description = 'ML deployment for SUN'
authors = [
    {name= 'Panagiotis Kasnesis', email= 'pkasnesis@thingenious.io'},
    {name= 'Lazaros Toumanidis', email= 'laztoum@protonmail.com'}
]
license= {text = 'MIT'}
dynamic = ['version']
readme = 'README.md'
requires-python = '~=3.12'
dependencies = [
    'numpy',
    'nvidia-pytriton==0.5.4; platform_system == "Linux" and platform_machine == "x86_64"',
    'torch==2.2.2',
]
[project.urls]
homepage = 'https://gitlab.com/thingenious/sun/posture'
repository = 'https://gitlab.com/thingenious/sun/posture'
[project.optional-dependencies]
dev = [
    'autoflake==2.3.1',
    'bandit==1.7.8',
    'black==24.3.0',
    'flake8==7.0.0',
    'isort==5.13.2',
    'mypy==1.9.0',
    'pre-commit==3.7.0',
    'pydocstyle==6.3.0',
    'pylint==3.1.0',
    'pylint-exit==1.2.0',
    'python-dotenv==1.0.1',
    'ruff==0.3.3',
    'toml==0.10.2',
    'types-PyYAML==6.0.12.20240311',
    'types-toml==0.10.8.20240310',
    'yamllint==1.35.1',
]
# black
[tool.black]
line-length = 120
skip-string-normalization=true
include = '''
    \.pyi?$
'''
exclude = '''
/(
    \.git
  | \.hg
  | \.mypy_cache
  | \.tox
  | \.venv
  | .local
  | _build
  | __init__.py
  | .local
)/
'''
extend-exclude = '''
/(
  | static/*
  | templates
)/
'''

# mypy
[tool.mypy]
files = '.'
# mypy_path = './app/'
platform = 'linux'
ignore_missing_imports = true
disallow_untyped_defs = true
warn_unused_ignores = false
follow_imports = 'skip'
exclude = [
    '.venv',
    '.local'
]
plugins = [
]
# isort
[tool.isort]
profile ='black'
skip=[
    '.venv',
    './.local'
]
include_trailing_comma=true
force_grid_wrap=0
use_parentheses=true
line_length=120
[tool.pydocstyle]
match-dir='([^!(.venv)].*)([^!(.local)].*)'

# pylint
[tool.pylint.master]
load-plugins = [
    'pylint.extensions.mccabe',
    'pylint.extensions.redefined_variable_type',
    'pylint.extensions.broad_try_clause',
    'pylint.extensions.no_self_use',
]
extension-pkg-whitelist= []
fail-under=8.0
ignore=["CVS"]
ignore-paths = [
    "^(.*)/.venv/*",
    "^(.*)/.local/.*",
    ".venv/*",
    ".local/.*"
]
unsafe-load-any-extension="no"

[tool.pylint.messages_control]
enable=["c-extension-no-member"]
[tool.pylint.fotmat]
max-line-length=120
[tool.pylint.similarities]
ignore-imports="yes"
ignore-signatures="yes"
min-similarity-lines=10
[tool.pylint.design]
max-args=9
max-attributes=9

# bandit
[tool.bandit]
exclude_dirs = [
    ".venv",
    ".local"
]
# B104: bind to all interfaces (0.0.0.0)
# B110: allow pass on try/except
# B404: allow import subprocess
# B602, B603: allow shell=True (subprocess,popen)
skips = ['B104', 'B110', 'B404', 'B602', 'B603' ]

# ruff
[tool.ruff]
line-length = 120
extend-exclude = []

[tool.ruff.lint]
select = ["E4", "E7", "E9", "F", "Q"]
ignore = []
# # Allow fix for all enabled rules (when `--fix`) is provided.
fixable = ["ALL"]
unfixable = []

# # Allow unused variables when underscore-prefixed.
dummy-variable-rgx = "^(_+|(_+[a-zA-Z0-9_]*[a-zA-Z0-9]+?))$"

[tool.ruff.format]
exclude = ["*.pyi", "*.j2"]
# Like Black, use double quotes for strings.
quote-style = "double"
# Like Black, indent with spaces, rather than tabs.
indent-style = "space"
# Like Black, respect magic trailing commas.
skip-magic-trailing-comma = false
line-ending = "lf"
