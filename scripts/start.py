"""Start a docker/podman container."""

import argparse
import os
import shutil
import subprocess  # nosemgrep # nosec
from pathlib import Path
from typing import List, Optional

_KEY_PREFIX = "POSTURE"
_DEFAULT_IMAGE_NAME = os.environ.get(f"{_KEY_PREFIX}_IMAGE_NAME", "posture")
_DEFAULT_TAG = os.environ.get(f"{_KEY_PREFIX}_IMAGE_TAG", None)
_DEFAULT_HTTP_PORT = int(os.environ.get(f"{_KEY_PREFIX}_HTTP_PORT", "8000"))
_DEFAULT_GRPC_PORT = int(os.environ.get(f"{_KEY_PREFIX}_GRPC_PORT", "8001"))
_DEFAULT_METRICS_PORT = int(os.environ.get(f"{_KEY_PREFIX}_METRICS_PORT", "8002"))
_DEFAULT_SAGEMAKER_PORT = int(os.environ.get(f"{_KEY_PREFIX}_SAGEMAKER_PORT", "8080"))
_DEFAULT_CONTAINER_NAME = os.environ.get(f"{_KEY_PREFIX}_CONTAINER_NAME", "posture")
_DEFAULT_BASE_IMAGE = os.environ.get(f"{_KEY_PREFIX}_BASE_IMAGE", "nvcr.io/nvidia/cuda:12.4.0-devel-ubuntu22.04")


def run_command(cmd: List[str], allow_error: bool = True) -> None:
    """Run command."""
    cwd = Path(__file__).parent.parent.resolve()
    try:
        subprocess.run(
            cmd,
            check=True,
            cwd=cwd,
            env=os.environ,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE if allow_error else subprocess.DEVNULL,
        )  # nosemgrep # nosec
    except subprocess.CalledProcessError as error:
        if allow_error:
            return
        raise RuntimeError(f"Error running command: {error}") from error


def get_default_container_command() -> str:
    """Get the container command."""
    from_env = os.environ.get("CONTAINER_COMMAND", "")
    if from_env and from_env in ["docker", "podman"]:
        return from_env
    # prefer podman over docker if found
    if shutil.which("podman"):
        return "podman"
    if not shutil.which("docker"):
        raise RuntimeError("Could not find docker or podman.")
    return "docker"


def cli() -> argparse.ArgumentParser:
    """Command line interface."""
    parser = argparse.ArgumentParser(description="Start a docker/podman container.")
    parser.add_argument(
        "--container-name",
        type=str,
        default=_DEFAULT_CONTAINER_NAME,
        help="Name of the container to use, (default: posture).",
    )
    parser.add_argument(
        "--image-name",
        type=str,
        default=_DEFAULT_IMAGE_NAME,
        help="Name of the image to use.",
    )
    parser.add_argument(
        "--image-tag",
        type=str,
        default=_DEFAULT_TAG,
        help="Tag of the image to use.",
    )
    parser.add_argument(
        "--http-port",
        type=int,
        default=_DEFAULT_HTTP_PORT,
        help=f"HTTP port to use, (default: {_DEFAULT_HTTP_PORT}).",
    )
    parser.add_argument(
        "--grpc-port",
        type=int,
        default=_DEFAULT_GRPC_PORT,
        help=f"gRPC port to use, (default: {_DEFAULT_GRPC_PORT}).",
    )
    parser.add_argument(
        "--metrics-port",
        type=int,
        default=_DEFAULT_METRICS_PORT,
        help=f"Metrics port to use, (default: {_DEFAULT_METRICS_PORT}).",
    )
    parser.add_argument(
        "--sagemaker-port",
        type=int,
        default=_DEFAULT_SAGEMAKER_PORT,
        help=f"Sagemaker port to use, (default: {_DEFAULT_SAGEMAKER_PORT}).",
    )
    default_container_command = get_default_container_command()
    parser.add_argument(
        "--container-command",
        default=default_container_command,
        choices=["docker", "podman"],
        help="The container command to use.",
    )
    parser.add_argument(
        "--debug",
        action="store_true",
        default=False,
        help="Enable debug mode.",
    )
    return parser


def get_port_args(args: argparse.Namespace) -> List[str]:
    """Get port arguments to include."""
    http_port = args.http_port
    grpc_port = args.grpc_port
    metrics_port = args.metrics_port
    sagemaker_port = args.sagemaker_port
    port_args = [
        "-p",
        f"{http_port}:{http_port}",
        "-p",
        f"{grpc_port}:{grpc_port}",
        "-p",
        f"{metrics_port}:{metrics_port}",
        "-p",
        f"{sagemaker_port}:{sagemaker_port}",
    ]
    return port_args


def get_env_args(args: argparse.Namespace) -> List[str]:
    """Get environment arguments to include."""
    env_args = [
        "-e",
        f"HTTP_PORT={args.http_port}",
        "-e",
        f"GRPC_PORT={args.grpc_port}",
        "-e",
        f"METRICS_PORT={args.metrics_port}",
        "-e",
        f"SAGEMAKER_PORT={args.sagemaker_port}",
    ]
    if args.debug is True:
        env_args.append("-e")
        env_args.append("DEBUG=true")
    return env_args


def get_gpu_args(container_cmd: str) -> List[str]:
    """Get gpu related arguments to use."""
    args = ["-e", "NVIDIA_VISIBLE_DEVICES=all"]
    if "docker" in container_cmd:
        args.extend(["--gpus", "all"])
    elif "podman" in container_cmd:
        args.extend(["--device", "nvidia.com/gpu=all"])
    return args


def _get_tag(tag: Optional[str]) -> str:
    """Get the tag."""
    if tag:
        return tag
    # nvcr.io/nvidia/cuda:12.3.2-devel-ubuntu22.04
    nv_tag = _DEFAULT_BASE_IMAGE.split(":")[-1]
    cuda_version = nv_tag.split("-")[0]
    return f"latest-cuda-{cuda_version}"


def main() -> None:
    """Parse command line args and start container."""
    args = cli().parse_args()
    container_cmd = args.container_command or get_default_container_command()
    # stop if already started
    run_command([container_cmd, "stop", args.container_name], allow_error=True)
    command = [container_cmd, "run", "--platform", "linux/amd64"]
    command.extend(["--rm", "-d", "--init", "--name", args.container_name])
    port_args = get_port_args(args)
    command.extend(port_args)
    gpu_args = get_gpu_args(container_cmd)
    command.extend(gpu_args)
    env_args = get_env_args(args)
    command.extend(env_args)
    image = f"{args.image_name}:{_get_tag(args.image_tag)}"
    command.append(image)
    print("Running:")
    print(" ".join(command))
    run_command(command)
    print(f"Use (with -f to follow): {container_cmd} logs {args.container_name}")
    print("To view the container's logs.")


if __name__ == "__main__":
    main()
