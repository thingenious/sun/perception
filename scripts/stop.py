"""Stop the posture docker container if it exists."""

import argparse
import os
import shutil
import subprocess  # nosemgrep # nosec
from pathlib import Path
from typing import List

try:
    from dotenv import load_dotenv
except ImportError:
    pass
else:
    load_dotenv()


_DEFAULT_CONTAINER_NAME = os.environ.get("POSTURE_CONTAINER_NAME", "posture")


def run_command(cmd: List[str], allow_error: bool = True) -> None:
    """Run command."""
    cwd = Path(__file__).parent.parent.resolve()
    try:
        subprocess.run(
            cmd,
            check=True,
            cwd=cwd,
            env=os.environ,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE if allow_error else subprocess.DEVNULL,
        )  # nosemgrep # nosec
    except subprocess.CalledProcessError as error:
        if allow_error:
            return
        raise RuntimeError(f"Error running command: {error}") from error


def get_default_container_command() -> str:
    """Get the container command."""
    from_env = os.environ.get("CONTAINER_COMMAND", "")
    if from_env and from_env in ["docker", "podman"]:
        return from_env
    if shutil.which("podman"):
        return "podman"
    if not shutil.which("docker"):
        raise RuntimeError("Could not find docker or podman.")
    return "docker"


def stop_container(container_command: str, container_name: str) -> None:
    """Stop the container."""
    run_command([container_command, "stop", container_name], allow_error=True)


def main() -> None:
    """Run the main function."""
    default_container_command = get_default_container_command()
    default_container_name = _DEFAULT_CONTAINER_NAME
    parser = argparse.ArgumentParser(description="Stop the posture model docker container if it exists.")
    parser.add_argument(
        "--container-command",
        default=default_container_command,
        choices=["docker", "podman"],
        help="The container command to use.",
    )
    parser.add_argument(
        "--container-name",
        default=default_container_name,
        help=f"The container name to stop (default: {default_container_name}).",
    )
    args = parser.parse_args()
    container_command = args.container_command or get_default_container_command()
    stop_container(container_command, container_name=args.container_name)


if __name__ == "__main__":
    main()
