"""Cleanup."""

import glob
import os
import shutil

DIR_PATTERNS = [
    "__pycache__",
    ".cache",
    ".pytest_cache",
    ".mypy_cache",
    ".ruff_cache",
    "*.egg-info",
    "build",
    "htmlcov",
]

FILE_PATTERNS = [
    "*.pyc",
    "*.pyo",
    "*.pyc~",
    "*.py~",
    "*~",
    ".*~",
    ".coverage*",
]


def main() -> None:
    """Cleanup unnecessary files and directories."""
    for pattern in DIR_PATTERNS:
        for dirpath in glob.glob(f"./**/{pattern}", recursive=True):
            print(f"removing {dirpath}")
            shutil.rmtree(dirpath)

    for pattern in FILE_PATTERNS:
        for filepath in glob.glob(f"./**/{pattern}", recursive=True):
            print(f"removing {filepath}")
            os.remove(filepath)


if __name__ == "__main__":
    main()
