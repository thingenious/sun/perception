"""Build docker/podman image."""

import argparse
import os
import shutil
import subprocess  # nosemgrep # nosec
import sys
from pathlib import Path
from typing import Optional

_IMAGE_CHOICES = (
    "nvcr.io/nvidia/cuda:12.4.0-devel-ubuntu22.04",
    "nvcr.io/nvidia/cuda:12.3.2-devel-ubuntu22.04",
    "nvcr.io/nvidia/cuda:12.3.0-devel-ubuntu22.04",
    "nvcr.io/nvidia/cuda:12.2.0-devel-ubuntu22.04",
    "nvcr.io/nvidia/cuda:12.1.1-cudnn8-devel-ubuntu22.04",
)

_KEY_PREFIX = "POSTURE"
_DEFAULT_NAME = os.environ.get(f"{_KEY_PREFIX}_IMAGE_NAME", "posture")
_DEFAULT_TAG = os.environ.get(f"{_KEY_PREFIX}_IMAGE_TAG", None)
_DEFAULT_SQUASH = os.environ.get(f"{_KEY_PREFIX}_SQUASH", "false").lower() == "true"
_DEFAULT_PUSH = os.environ.get(f"{_KEY_PREFIX}_PUSH", "false").lower() == "true"
_DEFAULT_NO_CACHE = os.environ.get(f"{_KEY_PREFIX}_NO_CACHE", "false").lower() == "true"
_DEFAULT_PLATFORM = os.environ.get(f"{_KEY_PREFIX}_PLATFORM", "linux/amd64")
_DEFAULT_BASE_IMAGE = os.environ.get(f"{_KEY_PREFIX}_BASE_IMAGE", _IMAGE_CHOICES[0])


def cli() -> argparse.ArgumentParser:
    """Command line interface."""
    parser = argparse.ArgumentParser(description="Build a podman/docker image.")
    parser.add_argument(
        "--name",
        type=str,
        default=_DEFAULT_NAME,
        help="Name of the image to build.",
    )
    parser.add_argument(
        "--tag",
        type=str,
        default=_DEFAULT_TAG,
        help="Tag of the image to build.",
    )
    parser.add_argument(
        "--build-arg",
        type=str,
        action="append",
        help="Build arguments.",
    )
    parser.add_argument(
        "--base-image",
        type=str,
        default=_DEFAULT_BASE_IMAGE,
        choices=_IMAGE_CHOICES,
        help="Base image to use.",
    )
    parser.add_argument(
        "--squash",
        action="store_true",
        default=_DEFAULT_SQUASH,
        help="Squash the image layers.",
    )
    parser.add_argument(
        "--push",
        action="store_true",
        default=_DEFAULT_PUSH,
        help="Push the image to the registry after building.",
    )
    parser.add_argument(
        "--no-cache",
        action="store_true",
        default=_DEFAULT_NO_CACHE,
        help="Do not use cache when building the image.",
    )
    parser.add_argument(
        "--platform",
        type=str,
        default=_DEFAULT_PLATFORM,
        help="Set platform if the image is multi-platform.",
    )
    parser.add_argument(
        "--container-command",
        default=get_container_cmd(),
        choices=["docker", "podman"],
        help="The container command to use.",
    )
    return parser


def get_container_cmd() -> str:
    """Get the container command."""
    from_env = os.environ.get("CONTAINER_COMMAND", "")
    if from_env and from_env in ["docker", "podman"]:
        return from_env
    # prefer podman over docker if found
    if shutil.which("podman"):
        return "podman"
    if not shutil.which("docker"):
        raise RuntimeError("Could not find docker or podman.")
    return "docker"


def build_image(
    cwd: Path,
    containerfile: str,
    args: argparse.Namespace,
) -> str:
    """Build the image."""
    tag = _get_tag(args.base_image, args.tag)
    name = args.name
    build_args = args.build_arg
    base_image = args.base_image
    squash = args.squash
    push = args.push
    no_cache = args.no_cache
    platform = args.platform
    build_args = build_args or []
    container_command = args.container_command or get_container_cmd()
    cmd = [
        container_command,
        "build",
        "-f",
        containerfile,
        "--tag",
        f"{name}:{tag}",
    ]
    # squash: bool = kwargs.get("squash", False)
    # push: bool = kwargs.get("push", False)
    # no_cache: bool = kwargs.get("no_cache", False)
    if squash is True:
        cmd.append("--squash")
    if no_cache is True:
        cmd.append("--no-cache")
    if platform:
        cmd.extend(["--platform", platform])
    for arg in build_args:
        cmd.extend(["--build-arg", arg])
    if "base_image" not in " ".join(build_args):
        cmd.extend(["--build-arg", f"BASE_IMAGE={base_image}"])
    cmd.append(".")
    print(f"Running: \n{' '.join(cmd)}\n")
    try:
        subprocess.run(cmd, check=True, cwd=str(cwd), env=os.environ)
    except subprocess.CalledProcessError as e:
        print(e.stderr)
        sys.exit(1)
    if push is True:
        subprocess.run([container_command, "push", f"{name}:{tag}"], check=True)
    return f"{name}:{tag}"


def _get_tag(base_image: str, tag: Optional[str]) -> str:
    """Get the tag."""
    if tag is not None and tag:
        return tag
    # nvcr.io/nvidia/cuda:12.3.2-devel-ubuntu22.04
    nv_tag = base_image.split(":")[-1]
    cuda_version = nv_tag.split("-")[0]
    return f"latest-cuda-{cuda_version}"


def main() -> None:
    """Run the build."""
    args = cli().parse_args()
    cwd = Path(__file__).parent.parent.resolve()
    containerfile = "Containerfile" if (cwd / "Containerfile").is_file() else "Dockerfile"
    if not (cwd / containerfile).is_file():
        raise FileNotFoundError(f"Could not find {containerfile}.")
    built_image = build_image(args=args, containerfile=containerfile, cwd=cwd)
    print(f"Built image: {built_image}")
    print("Example usage:")
    container_cmd = get_container_cmd()
    runtime_arg = "--gpus all" if container_cmd == "docker" else "--device nvidia.com/gpu=all"
    container_name = args.name.replace("_", "-").split("/")[-1]
    cmd = (
        f"{container_cmd} run -it"
        f" --rm {runtime_arg}"
        f" --name {container_name}"
        f" --platform {args.platform}"
        f" -e NVIDIA_VISIBLE_DEVICES=all"
        f" -p 8000:8000 {built_image}"
    )
    print(f"  {cmd}")


if __name__ == "__main__":
    main()
