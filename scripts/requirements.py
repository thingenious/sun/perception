"""Generate requirements txt files from pyproject.toml."""

import os
import shutil
from pathlib import Path
from typing import List

import toml

PYPROJECT_TOML = Path(__file__).parent.parent / "pyproject.toml"
DESTINATION_DIR = Path(__file__).parent.parent / "requirements"


def write_requirements(file_name: str, dependencies: List[str]) -> None:
    """Write requirements file."""
    DESTINATION_DIR.mkdir(parents=True, exist_ok=True)
    destination_file = DESTINATION_DIR / file_name
    with open(destination_file, "w", encoding="utf-8") as f_out:
        for dependency in dependencies:
            f_out.write(dependency + "\n")


def main() -> None:
    """Parse pyproject.toml and generate requirements."""
    if DESTINATION_DIR.is_dir():
        shutil.rmtree(DESTINATION_DIR)
    with open(PYPROJECT_TOML, "r", encoding="utf-8") as f_in:
        data = toml.load(f_in)
    if "project" not in data:
        print("No project found in toml file")
        return
    project_data = data["project"]
    written_files = []
    if "dependencies" in project_data:
        dependencies: List[str] = project_data["dependencies"]
        write_requirements("main.txt", sorted(dependencies))
        written_files.append("-r main.txt")
    if "optional-dependencies" in project_data:
        optional_dependencies = project_data["optional-dependencies"]
        if not isinstance(optional_dependencies, dict):
            print("Invalid optional dependencies.")
            return
        for key, deps in optional_dependencies.items():
            file_name = f"{key}.txt"
            write_requirements(file_name, sorted(deps))
            written_files.append(f"-r {file_name}")
    if len(written_files) > 1:
        write_requirements("all.txt", written_files)
        print("Generated files:")
        for item in sorted(os.listdir(DESTINATION_DIR)):
            relative = os.path.join(DESTINATION_DIR, item).replace(os.getcwd(), ".")
            print(f" {relative}")


if __name__ == "__main__":
    main()
