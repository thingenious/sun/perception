# Posture Model Server

Using Nvidia [PyTriton](https://triton-inference-server.github.io/pytriton)

## Available actions

Build:

```shell
make build
# or `python scripts/build.py`
# or using podman/docker directly:
#  `podman build \
#       -f Containerfile
#       --tag posture:latest-cuda-12.3.2
#       --platform linux/amd64
#       --build-arg BASE_IMAGE=nvcr.io/nvidia/cuda:12.3.0-devel-ubuntu22.04 .`
# possible base images:
# https://catalog.ngc.nvidia.com/orgs/nvidia/containers/cuda/tags
# e.g.
# nvcr.io/nvidia/cuda:12.4.0-devel-ubuntu22.04
# nvcr.io/nvidia/cuda:12.3.2-devel-ubuntu22.04
# nvcr.io/nvidia/cuda:12.2.0-devel-ubuntu22.04
# nvcr.io/nvidia/cuda:12.1.1-cudnn8-devel-ubuntu22.04
```

`python scripts/build.py --help`

```shell
# usage: build.py [-h] [--name NAME] [--tag TAG] [--build-arg BUILD_ARG] ...

# Build a podman/docker image.

# options:
#   -h, --help            show this help message and exit
#   --name NAME           Name of the image to build.
#   --tag TAG             Tag of the image to build.
#   --build-arg BUILD_ARG
#                         Build arguments.
#   --base-image {nvcr.io/nvidia/cuda:12.4.0-devel-ubuntu22.04,  
#                 nvcr.io/nvidia/cuda:12.3.2-devel-ubuntu22.04,  
#                 nvcr.io/nvidia/cuda:12.3.0-devel-ubuntu22.04,  
#                 nvcr.io/nvidia/cuda:12.2.0-devel-ubuntu22.04,
#                 nvcr.io/nvidia/cuda:12.1.1-cudnn8-devel-ubuntu22.04}
#                         Base image to use.
#   --squash              Squash the image layers.
#   --push                Push the image to the registry after building.
#   --no-cache            Do not use cache when building the image.
#   --platform PLATFORM   Set platform if the image is multi-platform.
#   --container-command {docker,podman}
#                         The container command to use.
```

Start:

```shell
make start
# `python scripts/start.py`
# or
# `podman run \
#   --platform linux/amd64 \
#   --rm -d --init 
#   --name posture \
#   -p 8000:8000 -p 8001:8001 -p 8002:8002 -p 8080:8080 \
#   -e NVIDIA_VISIBLE_DEVICES=all \
#   --device nvidia.com/gpu=all \
#   posture:latest-cuda-12.3.2`
# or
# `docker run \
#   --platform linux/amd64 \
#   --rm -d --init \
#   --name posture \
#   -p 8000:8000 -p 8001:8001 -p 8002:8002 -p 8080:8080 \
#   -e NVIDIA_VISIBLE_DEVICES=all \
#   --gpus all \
#   posture:latest-cuda-12.3.2
```

`python scripts/start.py --help`

```shell
# Start a docker/podman container.

# options:
#   -h, --help            show this help message and exit
#   --container-name CONTAINER_NAME
#                         Name of the container to use, (default: posture).
#   --image-name IMAGE_NAME
#                         Name of the image to use.
#   --image-tag IMAGE_TAG
#                         Tag of the image to use.
#   --http-port HTTP_PORT
#                         HTTP port to use, (default: 8000).
#   --grpc-port GRPC_PORT
#                         gRPC port to use, (default: 8001).
#   --metrics-port METRICS_PORT
#                         Metrics port to use, (default: 8002).
#   --sagemaker-port SAGEMAKER_PORT
#                         Sagemaker port to use, (default: 8080).
#   --container-command {docker,podman}
#                         The container command to use.
#   --debug               Enable debug mode.
#
```

Stop:

```shell
make stop
# or `python scripts/stop.py`
# or `podman/docker stop posture`
```

`python scripts/stop.py --help`

```shell
# Stop the posture model docker container if it exists.

# options:
#   -h, --help            show this help message and exit
#   --container-command {docker,podman}
#                         The container command to use.
#   --container-name CONTAINER_NAME
#                         The container name to stop (default: posture).
```
