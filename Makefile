.DEFAULT_GOAL := help

.PHONY: help
help:
	@echo "Usage: make [target]"
	@echo ""
	@echo "Default target: help"
	@echo ""
	@echo "Targets:"
	@echo " help            Show this message and exit"
	@echo " format          Format the code"
	@echo " lint            Lint the code"
	@echo " forlint         Alias for 'make format && make lint'"
	@echo " clean           Remove unneeded files (__pycache__, .mypy_cache, etc.)"
	@echo " requirements    Generate requirements txt files from pyproject.toml"
	@echo " build           Build the docker image"
	@echo " start           Start a docker container"
	@echo " start-debug     Start a docker container in debug mode"
	@echo " stop            Stop the docker container"
	@echo " restart         Alias for 'make stop && make start'"
	@echo " dev             Start the python server"
	@echo ""

.PHONY: format
format:
	isort .
	autoflake --remove-all-unused-imports --remove-unused-variables --in-place .
	black --config pyproject.toml .
	ruff format --config pyproject.toml .

.PHONY: lint
lint:
	isort --check-only .
	black --check --config pyproject.toml .
	mypy --config pyproject.toml .
	flake8 --config=.flake8
	pydocstyle --config pyproject.toml .
	bandit -c pyproject.toml -r .
	yamllint -c .yamllint.yaml .
	ruff check --config pyproject.toml .
	pylint --rcfile=pyproject.toml --recursive y --output-format=text app/

.PHONY: forlint
forlint: format lint

.PHONY: clean
clean:
	python scripts/clean.py

.PHONY: requirements
requirements:
	python -m pip install toml
	python scripts/requirements.py

.PHONY: build
build:
	python scripts/build.py

.PHONY: start
start: build
	python scripts/start.py

.PHONY: start-debug
start-debug: build
	python scripts/start.py --debug

.PHONY: stop
stop:
	python scripts/stop.py

.PHONY: restart
restart: stop start

.PHONY: dev
dev:
	python -m app
