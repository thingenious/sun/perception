"""Application entrypoint."""

import os
import sys
from pathlib import Path

try:
    from app.server import serve
except ImportError:
    sys.path.append(str(Path(__file__).parent.parent))
    from app.server import serve


def main():
    # type: () -> None
    """Application entrypoint."""
    http_port = int(os.environ.get("HTTP_PORT", "8000"))
    grpc_port = int(os.environ.get("GRPC_PORT", "8001"))
    metrics_port = int(os.environ.get("METRICS_PORT", "8002"))
    sagemaker_port = int(os.environ.get("SAGEMAKER_PORT", "8080"))
    allow_http = os.environ.get("ALLOW_HTTP", "true").lower() != "false"
    allow_grpc = os.environ.get("ALLOW_GRPC", "true").lower() != "false"
    allow_metrics = os.environ.get("ALLOW_METRICS", "true").lower() != "false"
    allow_sagemaker = os.environ.get("ALLOW_SAGEMAKER", "true").lower() != "false"
    # for SSL/TLS, check if not none and if the files exist, modify the variables accordingly
    grpc_use_ssl = os.environ.get("GRPC_USE_SSL", "false").lower() == "true"  # type: bool | None
    grpc_use_ssl_mutual = os.environ.get("GRPC_USE_SSL_MUTUAL", "false").lower() == "true"  # type: bool | None
    grpc_root_cert = os.environ.get("GRPC_ROOT_CERT", None)
    grpc_server_cert = os.environ.get("GRPC_SERVER_CERT", None)
    grpc_server_key = os.environ.get("GRPC_SERVER_KEY", None)
    log_verbose = os.environ.get("DEBUG", "false").lower() in (
        "true",
        "1",
        "yes",
        "on",
        "t",
        "y",
    )
    if grpc_root_cert and not Path(grpc_root_cert).exists():
        grpc_root_cert = None
    if grpc_server_cert and not Path(grpc_server_cert).exists():
        grpc_server_cert = None
    if grpc_server_key and not Path(grpc_server_key).exists():
        grpc_server_key = None
    if grpc_root_cert is None or grpc_server_cert is None or grpc_server_key is None:
        grpc_use_ssl = None
        grpc_use_ssl_mutual = None
    serve(
        http_port=http_port,
        grpc_port=grpc_port,
        metrics_port=metrics_port,
        sagemaker_port=sagemaker_port,
        allow_http=allow_http,
        allow_grpc=allow_grpc,
        allow_metrics=allow_metrics,
        allow_sagemaker=allow_sagemaker,
        grpc_use_ssl=grpc_use_ssl,
        grpc_use_ssl_mutual=grpc_use_ssl_mutual,
        grpc_root_cert=grpc_root_cert,
        grpc_server_cert=grpc_server_cert,
        grpc_server_key=grpc_server_key,
        log_verbose=log_verbose,
    )


if __name__ == "__main__":
    main()
