"""Pytorch models for the project."""

from .posture_ext import PostureExtNet
from .posture_net import PostureNet
from .posture_squat_net import PostureSquatNet
from .range_of_motion_net import RangeOfMotionNet

__all__ = [
    "PostureNet",
    "PostureExtNet",
    "PostureSquatNet",
    "RangeOfMotionNet",
]
