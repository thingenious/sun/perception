"""Model definition."""

# pylint: disable=invalid-name,too-many-arguments,too-many-instance-attributes,duplicate-code

from .posture_net import PostureNet


class PostureExtNet(PostureNet):
    """Posture/extension net."""
