"""Model definition."""

# pylint: disable=invalid-name,too-many-arguments,too-many-instance-attributes,duplicate-code
from typing import List, Tuple

import torch
import torch.nn.functional as F
from torch import nn


class PostureNet(nn.Module):
    """Posture net."""

    # pylint: disable=dangerous-default-value
    def __init__(
        self,
        conv_blocks: int = 3,
        conv_layers: int = 3,
        in_channels: List[int] = [1, 32, 48],
        out_channels: List[int] = [32, 48, 64],
        kernel_size: List[Tuple[int, int]] = [(1, 11), (1, 11), (1, 11)],
        dilation: int = 1,
        padding: List[Tuple[int, int]] = [(0, 6), (0, 6), (0, 6)],
        dropout: List[float] = [0.5, 0.5, 0.5],
        pooling_size: List[Tuple[int, int]] = [(1, 8), (1, 4), (1, 4)],
        imu_pooling_size: List[Tuple[int, int]] = [(1, 8), (1, 4), (1, 2)],
        dense_out: int = 64,
        out_features: int = 8,
    ) -> None:
        """Initialize the model instance."""
        super().__init__()

        # hyperparameters
        self.conv_blocks = conv_blocks
        self.conv_layers = conv_layers
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.dilation = dilation
        self.padding = padding
        self.dropout = dropout
        self.pooling_size = pooling_size
        self.imu_pooling_size = imu_pooling_size
        self.dense_out = dense_out

        self.conv11 = nn.Conv2d(
            in_channels=self.in_channels[0],
            out_channels=self.out_channels[0],
            kernel_size=self.kernel_size[0],
            dilation=self.dilation,
            padding=self.padding[0],
        )
        self.conv21 = nn.Conv2d(
            in_channels=self.in_channels[1],
            out_channels=self.out_channels[1],
            kernel_size=self.kernel_size[1],
            dilation=self.dilation,
            padding=self.padding[1],
        )
        self.conv31 = nn.Conv2d(
            in_channels=self.in_channels[2],
            out_channels=self.out_channels[2],
            kernel_size=self.kernel_size[2],
            dilation=self.dilation,
            padding=self.padding[2],
        )
        self.conv41 = nn.Conv2d(
            in_channels=self.out_channels[2],
            out_channels=self.out_channels[2],
            kernel_size=self.kernel_size[2],
            dilation=self.dilation,
            padding=self.padding[2],
        )

        self.conv11e = nn.Conv2d(
            in_channels=self.in_channels[0],
            out_channels=self.out_channels[0],
            kernel_size=self.kernel_size[0],
            dilation=self.dilation,
            padding=self.padding[0],
        )
        self.conv21e = nn.Conv2d(
            in_channels=self.in_channels[1],
            out_channels=self.out_channels[1],
            kernel_size=self.kernel_size[1],
            dilation=self.dilation,
            padding=self.padding[1],
        )
        self.conv31e = nn.Conv2d(
            in_channels=self.in_channels[2],
            out_channels=self.out_channels[2],
            kernel_size=self.kernel_size[2],
            dilation=self.dilation,
            padding=self.padding[2],
        )
        self.conv41e = nn.Conv2d(
            in_channels=self.out_channels[2],
            out_channels=self.out_channels[2],
            kernel_size=self.kernel_size[2],
            dilation=self.dilation,
            padding=self.padding[2],
        )
        self.conv51e = nn.Conv2d(
            in_channels=self.out_channels[2],
            out_channels=self.out_channels[2],
            kernel_size=(8, 1),
            dilation=1,
        )
        self.conv61e = nn.Conv2d(
            in_channels=self.out_channels[2],
            out_channels=self.out_channels[2],
            kernel_size=(48, 1),
            dilation=1,
        )

        self.conv71e = nn.Conv2d(
            in_channels=self.out_channels[2],
            out_channels=self.out_channels[2],
            kernel_size=(56, 1),
            dilation=1,
        )

        self.flatten = nn.Flatten()
        self.dense1 = nn.Linear(288, self.dense_out)
        self.dense2 = nn.Linear(704, self.dense_out)
        self.out = nn.Linear(64, out_features)

        self.dropout1 = nn.Dropout(self.dropout[0])
        self.dropout2 = nn.Dropout(self.dropout[1])
        self.dropout3 = nn.Dropout(self.dropout[2])

        self.dropout1e = nn.Dropout(self.dropout[0])
        self.dropout2e = nn.Dropout(self.dropout[1])
        self.dropout3e = nn.Dropout(self.dropout[2])
        self.dropout4e = nn.Dropout(self.dropout[2])
        self.dropout5e = nn.Dropout(self.dropout[2])
        self.dropout6e = nn.Dropout(self.dropout[2])

        self.multihead_attnx = nn.MultiheadAttention(
            embed_dim=11,
            num_heads=1,
            batch_first=True,
            dropout=0.0,
        )

        self.layer_normx = nn.LayerNorm(11)

    def forward(self, x: torch.Tensor, e: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor | None]:
        """Forward pass of the model."""
        x1, e1 = self.emb(x, e)

        x = torch.cat((x1, e1), 2)

        x = F.relu(self.conv71e(x))

        attn_output_x = self.flatten(x)

        out2 = self.dense2(attn_output_x)

        out = self.out(out2)

        return out, None  # attn_output_weights

    def emb(self, x: torch.Tensor, e: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
        """Emb part."""
        e = e[:, :, :, :]

        x = F.max_pool2d(
            F.relu(self.conv11(x)),
            kernel_size=self.pooling_size[0],
            stride=self.pooling_size[0],
            ceil_mode=True,
        )
        x = self.dropout1(x)

        x = F.max_pool2d(
            F.relu(self.conv21(x)),
            kernel_size=self.pooling_size[1],
            stride=self.pooling_size[1],
            ceil_mode=True,
        )
        x = self.dropout2(x)

        x = F.max_pool2d(
            F.relu(self.conv31(x)),
            kernel_size=self.pooling_size[2],
            stride=self.pooling_size[2],
            ceil_mode=True,
        )
        x = self.dropout3(x)

        x = F.max_pool2d(
            F.relu(self.conv41(x)),
            kernel_size=self.pooling_size[2],
            stride=self.pooling_size[2],
            ceil_mode=True,
        )
        x = self.dropout4e(x)

        # #############################################################################
        e = F.max_pool2d(
            F.relu(self.conv11e(e)),
            kernel_size=self.imu_pooling_size[0],
            stride=self.imu_pooling_size[0],
            ceil_mode=True,
        )
        e = self.dropout1e(e)

        e = F.max_pool2d(
            F.relu(self.conv21e(e)),
            kernel_size=self.imu_pooling_size[1],
            stride=self.imu_pooling_size[1],
            ceil_mode=True,
        )
        e = self.dropout2e(e)

        e = F.max_pool2d(
            F.relu(self.conv31e(e)),
            kernel_size=self.imu_pooling_size[2],
            stride=self.imu_pooling_size[2],
            ceil_mode=True,
        )
        e = self.dropout3e(e)

        return x, e
