"""Range of motion model definition."""

# pylint: disable=invalid-name,too-many-arguments,too-many-instance-attributes,duplicate-code
from typing import List, Tuple

import torch
import torch.nn.functional as F
from torch import nn


class RangeOfMotionNet(nn.Module):
    """Range of motion model."""

    # pylint: disable=dangerous-default-value
    def __init__(
        self,
        conv_blocks: int = 3,
        conv_layers: int = 3,
        in_channels: List[int] = [1, 16, 16],
        out_channels: List[int] = [16, 16, 16],
        kernel_size: List[Tuple[int, int]] = [(1, 11), (1, 11), (1, 11)],
        dilation: int = 1,
        padding: List[Tuple[int, int]] = [(0, 6), (0, 6), (0, 6)],
        dropout: List[float] = [0.5, 0.5, 0.5],
        pooling_size: List[Tuple[int, int]] = [(1, 8), (1, 4), (1, 4)],
        imu_pooling_size: List[Tuple[int, int]] = [(1, 4), (1, 2), (1, 2)],
        dense_out: int = 32,
    ) -> None:
        """Initialize the model instance."""
        super().__init__()

        # hyperparameters
        self.conv_blocks = conv_blocks
        self.conv_layers = conv_layers
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.dilation = dilation
        self.padding = padding
        self.dropout = dropout
        self.pooling_size = pooling_size
        self.imu_pooling_size = imu_pooling_size
        self.dense_out = dense_out

        self.conv11 = nn.Conv2d(
            in_channels=self.in_channels[0],
            out_channels=self.out_channels[0],
            kernel_size=self.kernel_size[0],
            dilation=self.dilation,
            padding=self.padding[0],
        )
        self.conv21 = nn.Conv2d(
            in_channels=self.in_channels[1],
            out_channels=self.out_channels[1],
            kernel_size=self.kernel_size[1],
            dilation=self.dilation,
            padding=self.padding[1],
        )
        self.conv31 = nn.Conv2d(
            in_channels=self.in_channels[2],
            out_channels=self.out_channels[2],
            kernel_size=self.kernel_size[2],
            dilation=self.dilation,
            padding=self.padding[2],
        )
        self.conv41 = nn.Conv2d(
            in_channels=self.out_channels[2],
            out_channels=self.out_channels[2],
            kernel_size=self.kernel_size[2],
            dilation=self.dilation,
            padding=self.padding[2],
        )

        self.conv11e = nn.Conv2d(
            in_channels=self.in_channels[0],
            out_channels=self.out_channels[0],
            kernel_size=self.kernel_size[0],
            dilation=self.dilation,
            padding=self.padding[0],
        )
        self.conv21e = nn.Conv2d(
            in_channels=self.in_channels[1],
            out_channels=self.out_channels[1],
            kernel_size=self.kernel_size[1],
            dilation=self.dilation,
            padding=self.padding[1],
        )
        self.conv31e = nn.Conv2d(
            in_channels=self.in_channels[2],
            out_channels=self.out_channels[2],
            kernel_size=self.kernel_size[2],
            dilation=self.dilation,
            padding=self.padding[2],
        )
        self.conv41e = nn.Conv2d(
            in_channels=self.out_channels[2],
            out_channels=self.out_channels[2],
            kernel_size=self.kernel_size[2],
            dilation=self.dilation,
            padding=self.padding[2],
        )
        self.conv51e = nn.Conv2d(
            in_channels=self.out_channels[2],
            out_channels=self.out_channels[2],
            kernel_size=(8, 1),
            dilation=1,
        )
        self.conv61e = nn.Conv2d(
            in_channels=self.out_channels[2],
            out_channels=self.out_channels[2],
            kernel_size=(24, 1),
            dilation=1,
        )

        self.flatten = nn.Flatten()
        # self.dense1 = nn.Linear(288,self.dense_out)
        self.dense2 = nn.Linear(2384, self.dense_out)
        self.out = nn.Linear(self.dense_out * 1, 1)  # +self.dense_out

        self.dropout1 = nn.Dropout(self.dropout[0])
        self.dropout2 = nn.Dropout(self.dropout[1])
        self.dropout3 = nn.Dropout(self.dropout[2])

        self.dropout1e = nn.Dropout(self.dropout[0])
        self.dropout2e = nn.Dropout(self.dropout[1])
        self.dropout3e = nn.Dropout(self.dropout[2])
        self.dropout4e = nn.Dropout(self.dropout[2])
        self.dropout5e = nn.Dropout(self.dropout[2])
        self.dropout6e = nn.Dropout(self.dropout[2])

    def forward(self, x: torch.Tensor, e: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor | None]:
        """Forward pass of the model."""
        x, e = self.emb(x, e)
        flat2 = self.flatten(e)
        out2 = self.dense2(flat2)
        out = self.out(out2)
        return out, None

    def emb(self, x: torch.Tensor, e: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
        """Emb part."""
        e = F.max_pool2d(
            F.relu(self.conv11e(e)),
            kernel_size=self.imu_pooling_size[0],
            stride=self.imu_pooling_size[0],
            ceil_mode=True,
        )
        e = self.dropout1e(e)

        e = F.relu(self.conv61e(e))
        e = self.dropout6e(e)

        return x, e
