"""Triton server for the posture models."""

from typing import Any, Dict, List

import numpy as np
import numpy.typing as npt

# pylint: disable=import-error
# pyright: reportMissingImports=false
from pytriton.model_config import DynamicBatcher, ModelConfig, Tensor  # type: ignore
from pytriton.proxy.types import Request  # type: ignore
from pytriton.triton import Triton, TritonConfig  # type: ignore

from app.config import MODEL_INPUTS, MODEL_NAME, MODEL_OUTPUTS, MODEL_VERSION
from app.runner import ModelRunner

Runner = ModelRunner()


def infer_fn(requests: List[Request]) -> List[Dict[str, npt.NDArray[np.int_] | npt.NDArray[np.float_]]]:
    """Inference function."""
    results = []
    emg_inputs = np.array([req.data["emg"] for req in requests], dtype=np.float32)
    imu_inputs = np.array([req.data["imu"] for req in requests], dtype=np.float32)
    total = len(emg_inputs)
    for index in range(total):
        infer_outputs = Runner.run(emg=emg_inputs[index], imu=imu_inputs[index])
        output_dict = {}
        for model_output, infer_output in zip(MODEL_OUTPUTS, infer_outputs):
            output_name = model_output["name"]
            output_dtype = model_output["dtype"]
            output_dict[output_name] = np.array([infer_output], dtype=output_dtype)
        results.append(output_dict)
    return results


def serve(
    http_port: int = 8000,
    **kwargs: Any,
) -> None:
    """Start the triton server."""
    triton_config = TritonConfig(
        http_port=http_port,
        grpc_port=kwargs.get("grpc_port", 8001),
        sagemaker_port=kwargs.get("sagemaker_port", 8080),
        metrics_port=kwargs.get("metrics_port", 8002),
        allow_http=kwargs.get("allow_http", False),
        allow_grpc=kwargs.get("allow_grpc", True),
        allow_metrics=kwargs.get("allow_metrics", True),
        allow_sagemaker=kwargs.get("allow_sagemaker", True),
        grpc_root_cert=kwargs.get("grpc_root_cert", None),
        grpc_server_cert=kwargs.get("grpc_server_cert", None),
        grpc_server_key=kwargs.get("grpc_server_key", None),
        grpc_use_ssl=kwargs.get("grpc_use_ssl", False),
        grpc_use_ssl_mutual=kwargs.get("grpc_use_ssl_mutual", False),
        log_verbose=kwargs.get("log_verbose", False),
    )
    model_config = ModelConfig(
        batching=True,
        max_batch_size=1,
        batcher=DynamicBatcher(),
        response_cache=False,
        decoupled=False,
    )
    with Triton(config=triton_config) as triton:
        triton.bind(
            model_name=MODEL_NAME,
            model_version=MODEL_VERSION,
            infer_func=infer_fn,
            inputs=[Tensor(name=inp["name"], dtype=inp["dtype"], shape=inp["shape"]) for inp in MODEL_INPUTS],
            outputs=[Tensor(name=out["name"], dtype=out["dtype"], shape=out["shape"]) for out in MODEL_OUTPUTS],
            config=model_config,
        )
        triton.serve()
