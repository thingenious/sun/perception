"""Model name, version, inputs and outputs configuration."""

from typing import Any, List, Tuple, TypedDict

import numpy as np

MODEL_NAME = "posture"
MODEL_VERSION = 1
WINDOW_SIZE_SECONDS = 4
EMG_SAMPLE_RATE = 1259.259
IMU_SAMPLE_RATE = 148.148
NUMBER_OF_SENSORS = 8
IMU_CHANNELS = 6  # acc_{x,y,z}, gyro_{x,y,z}
EMG_SIZE = int(round(EMG_SAMPLE_RATE * WINDOW_SIZE_SECONDS))  # 5 sensors: 6296, 4 sensors: 5037
IMU_SIZE = int(round(IMU_SAMPLE_RATE * WINDOW_SIZE_SECONDS))  # 5 sensors: 741, 4 sensors: 593

EMG_INPUT_SHAPE = (1, NUMBER_OF_SENSORS, EMG_SIZE)
IMU_INPUT_SHAPE = (1, NUMBER_OF_SENSORS * IMU_CHANNELS, IMU_SIZE)


class IODict(TypedDict):
    """Input dictionary type."""

    name: str
    shape: Tuple[int, ...]
    dtype: Any
    datatype: str


MODEL_INPUTS: List[IODict] = [
    {"name": "emg", "shape": EMG_INPUT_SHAPE, "dtype": np.float32, "datatype": "FP32"},
    {"name": "imu", "shape": IMU_INPUT_SHAPE, "dtype": np.float32, "datatype": "FP32"},
]
MODEL_OUTPUTS: List[IODict] = [
    {"name": "prediction", "shape": (1,), "dtype": np.int8, "datatype": "INT8"},
    {"name": "confidence", "shape": (1,), "dtype": np.float32, "datatype": "FP32"},
    {"name": "left", "shape": (1,), "dtype": np.float32, "datatype": "FP32"},
    {"name": "right", "shape": (1,), "dtype": np.float32, "datatype": "FP32"},
]

__all__ = [
    "MODEL_NAME",
    "MODEL_VERSION",
    "MODEL_INPUTS",
    "MODEL_OUTPUTS",
]
