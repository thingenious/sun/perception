"""Normalize model inputs."""

from .normalize import (
    normalize_posture_ext_input,
    normalize_posture_input,
    normalize_posture_squat_input,
    normalize_rom_input,
)

__all__ = [
    "normalize_posture_input",
    "normalize_posture_ext_input",
    "normalize_posture_squat_input",
    "normalize_rom_input",
]
