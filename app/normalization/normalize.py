"""Normalization utils."""

from typing import List, Tuple

import numpy as np
import numpy.typing as npt

from .posture_ext_normalization import (
    POSTURE_EXT_EMG_MEAN,
    POSTURE_EXT_EMG_STD,
    POSTURE_EXT_IMU_MEAN,
    POSTURE_EXT_IMU_STD,
)
from .posture_normalization import POSTURE_EMG_MEAN, POSTURE_EMG_STD, POSTURE_IMU_MEAN, POSTURE_IMU_STD
from .posture_squat_normalization import (
    POSTURE_SQUAT_EMG_MEAN,
    POSTURE_SQUAT_EMG_STD,
    POSTURE_SQUAT_IMU_MEAN,
    POSTURE_SQUAT_IMU_STD,
)
from .range_of_motion_normalization import ROM_IMU_MEAN, ROM_IMU_STD


def _normalize_signal_data(
    signal_data: npt.NDArray[np.float_],
    mean_values: List[float],
    std_values: List[float],
) -> npt.NDArray[np.float_]:
    """Normalize signal data."""
    signal_copy = signal_data.copy()
    for index, mean in enumerate(mean_values):
        std = std_values[index]
        signal_copy[0, 0, index, :] = (signal_data[0, 0, index, :] - mean) / std
    return signal_copy.astype(np.float32)


def normalize_posture_input(
    emg_data: npt.NDArray[np.float_],
    imu_data: npt.NDArray[np.float_],
) -> Tuple[npt.NDArray[np.float_], npt.NDArray[np.float_]]:
    """Normalize posture input data."""
    normalized_emg = _normalize_signal_data(
        signal_data=emg_data,
        mean_values=POSTURE_EMG_MEAN,
        std_values=POSTURE_EMG_STD,
    )
    normalized_imu = _normalize_signal_data(
        signal_data=imu_data,
        mean_values=POSTURE_IMU_MEAN,
        std_values=POSTURE_IMU_STD,
    )
    return normalized_emg, normalized_imu


def normalize_posture_squat_input(
    emg_data: npt.NDArray[np.float_],
    imu_data: npt.NDArray[np.float_],
) -> Tuple[npt.NDArray[np.float_], npt.NDArray[np.float_]]:
    """Normalize posture squat input data."""
    normalized_emg = _normalize_signal_data(
        signal_data=emg_data,
        mean_values=POSTURE_SQUAT_EMG_MEAN,
        std_values=POSTURE_SQUAT_EMG_STD,
    )
    normalized_imu = _normalize_signal_data(
        signal_data=imu_data,
        mean_values=POSTURE_SQUAT_IMU_MEAN,
        std_values=POSTURE_SQUAT_IMU_STD,
    )
    return normalized_emg, normalized_imu


def normalize_posture_ext_input(
    emg_data: npt.NDArray[np.float_],
    imu_data: npt.NDArray[np.float_],
) -> Tuple[npt.NDArray[np.float_], npt.NDArray[np.float_]]:
    """Normalize posture extension input data."""
    normalized_emg = _normalize_signal_data(
        signal_data=emg_data,
        mean_values=POSTURE_EXT_EMG_MEAN,
        std_values=POSTURE_EXT_EMG_STD,
    )
    normalized_imu = _normalize_signal_data(
        signal_data=imu_data,
        mean_values=POSTURE_EXT_IMU_MEAN,
        std_values=POSTURE_EXT_IMU_STD,
    )
    return normalized_emg, normalized_imu


def normalize_rom_input(
    imu_data: npt.NDArray[np.float_],
) -> npt.NDArray[np.float_]:
    """Normalize range of motion input data."""
    normalized_rom_imu = _normalize_signal_data(
        signal_data=imu_data,
        mean_values=ROM_IMU_MEAN,
        std_values=ROM_IMU_STD,
    )
    return normalized_rom_imu
