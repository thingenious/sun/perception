"""Model runner."""

from pathlib import Path
from typing import Tuple

import numpy as np
import numpy.typing as npt
import torch
from torch.autograd import Variable

from .models import PostureExtNet, PostureNet, PostureSquatNet, RangeOfMotionNet
from .normalization import (
    normalize_posture_ext_input,
    normalize_posture_input,
    normalize_posture_squat_input,
    normalize_rom_input,
)

MODELS_DIR = Path(__file__).parent.parent / "models"
PREDICTION_SQUAT = 0
PREDICTION_SQUAT_FRONT_LEG = 2
PREDICTION_EXTENSION = 3


class ModelRunner:
    """Model runner class."""

    def __init__(self) -> None:
        """Initialize ths runner instance."""
        # posture_model_path = self.get_model_path("posture")
        device_name = "cuda" if torch.cuda.is_available() else "cpu"
        self.device = torch.device(device_name)
        self.posture_model = PostureNet()
        self.posture_ext_model = PostureExtNet()
        self.range_of_motion_model = RangeOfMotionNet()
        self.posture_squat_model = PostureSquatNet()
        self.load_models()

    def _load_posture_model(self) -> None:
        """Load the posture model."""
        posture_model_path = MODELS_DIR / "posture.pt"
        self.posture_model.load_state_dict(torch.load(posture_model_path, map_location=self.device))
        self.posture_model = self.posture_model.to(self.device)
        self.posture_model.eval()

    def _load_posture_squat_model(self) -> None:
        """Load the posture squat model."""
        posture_squat_model_path = MODELS_DIR / "posture_squat.pt"
        self.posture_squat_model.load_state_dict(torch.load(posture_squat_model_path, map_location=self.device))
        self.posture_squat_model = self.posture_squat_model.to(self.device)
        self.posture_squat_model.eval()

    def _load_posture_extension_model(self) -> None:
        """Load the posture extension model."""
        posture_ext_model_path = MODELS_DIR / "posture_ext.pt"
        self.posture_ext_model.load_state_dict(torch.load(posture_ext_model_path, map_location=self.device))
        self.posture_ext_model = self.posture_ext_model.to(self.device)
        self.posture_ext_model.eval()

    def _load_range_of_motion_model(self) -> None:
        """Load the range of motion model."""
        range_of_motion_model_path = MODELS_DIR / "range_of_motion.pt"
        self.range_of_motion_model.load_state_dict(torch.load(range_of_motion_model_path, map_location=self.device))
        self.range_of_motion_model = self.range_of_motion_model.to(self.device)
        self.range_of_motion_model.eval()

    def load_models(self) -> None:
        """Load the models."""
        self._load_posture_model()
        self._load_range_of_motion_model()
        self._load_posture_squat_model()

    def _get_posture_squat(self, emg: npt.NDArray[np.float_], imu: npt.NDArray[np.float_]) -> Tuple[int, float]:
        """Get the posture squat prediction."""
        normalized_emg, normalized_imu = normalize_posture_squat_input(emg_data=emg, imu_data=imu)
        with torch.no_grad():
            input_tensors = [
                Variable(torch.from_numpy(input_)).float().to(self.device)
                for input_ in [normalized_emg, normalized_imu]
            ]
            posture_output, _ = self.posture_squat_model(*input_tensors)
            prediction_out = torch.argmax(torch.softmax(posture_output, dim=1), 1)
            confidence = torch.max(torch.softmax(posture_output, dim=1)).item()
            prediction = prediction_out.detach().cpu().numpy().item()
        return prediction, confidence

    def _get_posture_ext(self, emg: npt.NDArray[np.float_], imu: npt.NDArray[np.float_]) -> Tuple[int, float]:
        """Get the posture extension prediction."""
        normalized_emg, normalized_imu = normalize_posture_ext_input(emg_data=emg, imu_data=imu)
        with torch.no_grad():
            input_tensors = [
                Variable(torch.from_numpy(input_)).float().to(self.device)
                for input_ in [normalized_emg, normalized_imu]
            ]
            posture_output, _ = self.posture_ext_model(*input_tensors)
            prediction_out = torch.argmax(torch.softmax(posture_output, dim=1), 1)
            confidence = torch.max(torch.softmax(posture_output, dim=1)).item()
            prediction = prediction_out.detach().cpu().numpy().item()
        return prediction, confidence

    def _get_posture(self, emg: npt.NDArray[np.float_], imu: npt.NDArray[np.float_]) -> Tuple[int, float]:
        """Get the posture prediction."""
        normalized_emg, normalized_imu = normalize_posture_input(emg_data=emg, imu_data=imu)
        with torch.no_grad():
            input_tensors = [
                Variable(torch.from_numpy(input_)).float().to(self.device)
                for input_ in [normalized_emg, normalized_imu]
            ]
            posture_output, _ = self.posture_model(*input_tensors)
            prediction_out = torch.argmax(torch.softmax(posture_output, dim=1), 1)
            confidence = torch.max(torch.softmax(posture_output, dim=1)).item()
            prediction = prediction_out.detach().cpu().numpy().item()
        if prediction > 5:
            # 6: OTHER, 7: WALKING, no further processing needed
            return prediction, confidence
        if prediction == PREDICTION_SQUAT:
            # 0, check if 1: muscle or 2: front leg
            prediction, confidence = self._get_posture_squat(emg=emg, imu=imu)
            if prediction != PREDICTION_SQUAT_FRONT_LEG:
                # check for squat_muscle / emg error
                # return prediction, confidence
                return prediction, confidence
        elif prediction == PREDICTION_EXTENSION:
            # 3, check if 4: extension_raised_leg or 5: extension_not_full
            # check for extension_raised_leg or extension_not_full
            # prediction, confidence = self._get_posture_ext(emg=emg, imu=imu)
            return prediction, confidence
        return prediction, confidence

    def _get_range_of_motion(self, emg: npt.NDArray[np.float_], imu: npt.NDArray[np.float_]) -> Tuple[float, float]:
        """Get the range of motion prediction."""
        right_imu = imu[:, :, :24, :]
        left_imu = imu[:, :, 24:, :]
        normalized_right = normalize_rom_input(imu_data=right_imu)
        normalized_left = normalize_rom_input(imu_data=left_imu)
        with torch.no_grad():
            right_input = Variable(torch.from_numpy(normalized_right)).float().to(self.device)
            left_input = Variable(torch.from_numpy(normalized_left)).float().to(self.device)
            emg_input = Variable(torch.from_numpy(emg)).float().to(self.device)
            right_output, _ = self.range_of_motion_model(emg_input, right_input)
            left_output, _ = self.range_of_motion_model(emg_input, left_input)
            right = right_output.detach().cpu().numpy().item()
            left = left_output.detach().cpu().numpy().item()
        return left, right

    def run(self, emg: npt.NDArray[np.float_], imu: npt.NDArray[np.float_]) -> Tuple[int, float, float, float]:
        """Call the model and return the predicted class and the confidence."""
        prediction, confidence = self._get_posture(emg=emg, imu=imu)
        left, right = self._get_range_of_motion(emg=emg, imu=imu)
        return prediction, confidence, left, right
